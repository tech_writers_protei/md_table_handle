# -*- coding: utf-8 -*-
from typing import Iterable

from constants import TableElement
from errors import InvalidCellCoordinateError
from table_cell import TableCell


class TableStripe:
    def __init__(self, index: int, table_element: TableElement, cells: Iterable[TableCell] = None):
        if cells is None:
            cells: list[TableCell] = []

        self._index: int = index
        self._table_element: TableElement = table_element
        self._cells: list[TableCell] = [*cells]
        self._cells.sort()

    def __str__(self):
        raise NotImplementedError

    def __iter__(self):
        return iter(self._cells)

    def __len__(self):
        return len(self._cells)

    def __getitem__(self, item):
        if isinstance(item, int):
            if 0 <= item < len(self):
                return self._cells[item]
            else:
                raise KeyError
        else:
            return NotImplemented

    def __bool__(self):
        return self._index != -1

    def __contains__(self, item):
        if isinstance(item, TableCell):
            return item in self._cells
        else:
            return False

    @property
    def attr(self) -> str:
        return self._table_element.value

    def validate(self):
        if self._cells and any(getattr(cell, self.attr) != self._index for cell in iter(self)):
            raise InvalidCellCoordinateError

    def __add__(self, other):
        raise NotImplementedError

    def __delitem__(self, key):
        raise NotImplementedError

    def switch(self, __from: int, __to: int):
        raise NotImplementedError

    def get_ge(self, index: int) -> list[TableCell]:
        return list(filter(lambda x: getattr(self, self.attr) > index, self._cells))

    def texts(self) -> list[str]:
        return [cell.text for cell in iter(self)]

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        self._index = value

    def clear(self):
        for cell in iter(self):
            cell.clear()

    def duplicate(self):
        cells: list[TableCell] = [cell.duplicate() for cell in iter(self)]
        return self.__class__(self._index, self._table_element, cells)
