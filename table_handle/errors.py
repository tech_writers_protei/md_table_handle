# -*- coding: utf-8 -*-
class BaseError(Exception):
    """Base error class to inherit."""


class LineInvalidTypeError(BaseError):
    """Invalid line index."""


class InvalidCellCoordinateError(BaseError):
    """Cells in the table element have invalid coordinates."""


class MissingTableCellError(BaseError):
    """Cell is not found in the table element."""


class InvalidHeadingError(BaseError):
    """Table does not have this heading."""


class ValueOutOfRangeError(BaseError):
    """Value is invalid."""


class InvalidTableStripeLengthError(BaseError):
    """Table stripes have different lengths."""
