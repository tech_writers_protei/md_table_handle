# -*- coding: utf-8 -*-
from loguru import logger

from table_handle.dir_file import DirFile
from table_handle.table import Table
from table_handle.table_cell import TableCell
from table_handle.table_row import HeaderRow


class TableGenerator:
    def __init__(self, dir_file: DirFile):
        self._dir_file: DirFile = dir_file
        self._content: list[str] = []
        self._indexes: list[int] = []
        self._headers: list[int] = []
        self._header_rows: list[HeaderRow] = []

    def read(self):
        self._dir_file.read()
        self._content = self._dir_file.content

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._content[item]
        else:
            logger.error(f"Ключ {item} должен быть типа int, но получен {type(item)}")

    def set_indexes(self):
        self._indexes = [index for index, line in enumerate(iter(self)) if line.startswith("|")]

    def set_headers(self):
        raise NotImplementedError


class MdTableGenerator(TableGenerator):
    def set_headers(self):
        self._headers = [index for index, line in enumerate(iter(self)) if line.startswith(("|-", "|:-"))]

    def set_header_rows(self):
        for header_index in self._headers:
            _line: str = self._content[header_index].strip("|").strip()
            _items: list[str] = [el.strip() for el in _line.split("|")]

            cells: list[TableCell] = [TableCell(-1, index, _item) for index, _item in enumerate(_items)]
            header_row: HeaderRow = HeaderRow(cells)

            self._header_rows.append(header_row)


class AsciiDocGenerator(TableGenerator):
    def set_indexes(self):
        self._indexes = [index for index, line in enumerate(iter(self)) if line.startswith("|===")]

    def set_headers(self):
        self._headers = [index for index, line in enumerate(iter(self)) if line.startswith("|")]
