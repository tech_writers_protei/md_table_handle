# -*- coding: utf-8 -*-

from loguru import logger

from constants import TableCoordinate, validate_over_one_int


class TableCell:
    identifier: int = 0
    items = dict()

    def __init__(self, row: int, column: int, text: str = None):
        if text is None:
            text: str = ""

        self._row: int = row
        self._column: int = column
        self._text: str = text
        self._index: int = self.__class__.identifier

        self.__class__.items[self._index] = self
        self.__class__.identifier += 1

    @property
    def index(self):
        return self._index

    @classmethod
    def new(cls):
        return cls(-1, -1)

    @property
    def row(self):
        return self._row

    @row.setter
    def row(self, value):
        self._row = value

    @property
    def column(self):
        return self._column

    @column.setter
    def column(self, value):
        self._column = value

    @property
    def coord(self) -> TableCoordinate:
        return TableCoordinate(self._row, self._column)

    def validate(self):
        validate_over_one_int(self._row)
        validate_over_one_int(self._column)

    def __str__(self):
        return f"<{self.__class__.__name__}>({self._row}, {self._column})"

    __repr__ = __str__

    def __bool__(self):
        return self.coord != (-1, -1)

    def shift_hor(self, shift: int):
        self._column += shift

    def shift_vert(self, shift: int):
        self._row += shift

    def clear(self):
        self._text = ""

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = f"{value}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if bool(self) and bool(other):
                return self.coord == other.coord
            else:
                logger.info(f"По крайней мере одна из ячеек не привязана к таблице")
                return False
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            if bool(self) and bool(other):
                return self.coord != other.coord
            else:
                logger.info(f"По крайней мере одна из ячеек не привязана к таблице")
                return True
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            if self._row == other._row:
                return self._column < other.column
            elif self._column == other._column:
                return self._row < other._row
            else:
                return NotImplemented
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            if self._row == other._row:
                return self._column > other.column
            elif self._column == other._column:
                return self._row > other._row
            else:
                return NotImplemented
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            if self._row == other._row:
                return self._column <= other.column
            elif self._column == other._column:
                return self._row <= other._row
            else:
                return NotImplemented
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            if self._row == other._row:
                return self._column >= other.column
            elif self._column == other._column:
                return self._row >= other._row
            else:
                return NotImplemented
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, self.__class__):
            self._text = f"{self._text}\n{other._text}"
            del other
        elif isinstance(other, str):
            self._text = f"{self._text}\n{other}"
        else:
            logger.error(f"Невозможно добавить элемент {other} типа {type(other)}")

    def duplicate(self):
        return self.__class__(-1, -1, self._text)
