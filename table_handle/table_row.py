# -*- coding: utf-8 -*-
from typing import Iterable

from loguru import logger

from constants import TableElement, validate_under_limit_int
from errors import InvalidTableStripeLengthError, MissingTableCellError
from table_cell import TableCell
from table_stripe import TableStripe


class TableRow(TableStripe):
    def __init__(self, index: int, cells: Iterable[TableCell] = None):
        table_element: TableElement = TableElement.ROW
        super().__init__(index, table_element, cells)

    def __str__(self):
        _line: str = " | ".join(self.texts())
        return f"| {_line} |"

    def __add__(self, other):
        def shift(_column: int, table_cell: TableCell):
            for _cell in self.get_ge(_column):
                _cell.shift_hor(1)
            self._cells.insert(_column, table_cell)

        if isinstance(other, tuple) and len(other) == 2:
            index, text = other

            if isinstance(index, int) and isinstance(text, str):
                row: int = self._index
                column: int = index
                cell: TableCell = TableCell(row, column, text)
                shift(column, cell)

            else:
                logger.error(
                    f"Добавляемый элемент должен быть типа (int, str), но получен ({type(index)}, {type(text)})")

        elif isinstance(other, TableCell):
            other.row = self._index
            column: int = other.column
            shift(column, other)

        elif isinstance(other, self.__class__):
            if len(self) != len(other):
                logger.error(f"В исходной строке {len(self)} ячеек, в добавляемой строке {len(other)} ячеек")
                raise InvalidTableStripeLengthError
            else:
                for index in range(len(self)):
                    _origin_cell: TableCell = self[index]
                    _new_cell: TableCell = other[index]
                    _origin_cell + _new_cell

                del other

        else:
            logger.error(
                f"Добавляемый элемент {other} должен быть типа tuple с 2 элементами или TableCell, "
                f"но получен {type(other)}")

    def __delitem__(self, key):
        def shift(_column: int):
            for cell in self.get_ge(column):
                cell.shift_hor(-1)

        if isinstance(key, int):
            validate_under_limit_int(key, len(self))
            column: int = self._cells.pop(key).column
            shift(column)

        elif isinstance(key, TableCell):
            if key not in self:
                raise MissingTableCellError
            else:
                column: int = key.column

            shift(column)

        else:
            logger.error(
                f"Добавляемый элемент {key} должен быть типа int или TableCell, "
                f"но получен {type(key)}")

    def switch(self, __from: int, __to: int):
        validate_under_limit_int(__from, len(self))
        validate_under_limit_int(__to, len(self))
        _cell_from: TableCell = self[__from]
        _cell_to: TableCell = self[__to]

        _cell_from.column, _cell_to.column = _cell_to.column, _cell_from.column

    def shift_vert(self, shift: int):
        for cell in iter(self):
            cell.shift_vert(shift)
        self._index += shift


class HeaderRow(TableRow):
    def __init__(self, cells: Iterable[TableCell] = None):
        index: int = -1
        super().__init__(index, cells)
