# -*- coding: utf-8 -*-
from typing import Iterable

from loguru import logger

from constants import TableElement, validate_under_limit_int
from errors import InvalidTableStripeLengthError, MissingTableCellError
from table_cell import TableCell
from table_stripe import TableStripe


class TableColumn(TableStripe):
    def __init__(self, index: int, cells: Iterable[TableCell] = None):
        table_element: TableElement = TableElement.COLUMN
        super().__init__(index, table_element, cells)

    def __str__(self):
        _lines: list[str] = [f"| {text} |" for text in self.texts()]
        return "\n".join(_lines)

    def __add__(self, other):
        def shift(_row: int, table_cell: TableCell):
            for _cell in self.get_ge(_row):
                _cell.shift_vert(1)
            self._cells.insert(_row, table_cell)

        if isinstance(other, tuple) and len(other) == 2:
            index, text = other

            if isinstance(index, int) and isinstance(text, str):
                row: int = index
                column: int = self._index
                cell: TableCell = TableCell(row, column, text)
                shift(row, cell)

            else:
                logger.error(
                    f"Добавляемый элемент должен быть типа (int, str), но получен ({type(index)}, {type(text)})")

        elif isinstance(other, TableCell):
            other.column = self._index
            row: int = other.row
            shift(row, other)

        elif isinstance(other, self.__class__):
            if len(self) != len(other):
                logger.error(f"В исходном столбце {len(self)} ячеек, в добавляемом столбце {len(other)} ячеек")
                raise InvalidTableStripeLengthError
            else:
                for index in range(len(self)):
                    _origin_cell: TableCell = self[index]
                    _new_cell: TableCell = other[index]
                    _origin_cell + _new_cell

                del other

        else:
            logger.error(
                f"Добавляемый элемент {other} должен быть типа tuple с 2 элементами или TableCell, "
                f"но получен {type(other)}")

    def __delitem__(self, key):
        def shift(_row: int):
            for cell in self.get_ge(_row):
                cell.shift_vert(-1)

        if isinstance(key, int):
            validate_under_limit_int(key, len(self))
            row: int = self._cells.pop(key).row
            shift(row)

        elif isinstance(key, TableCell):
            if key not in self:
                raise MissingTableCellError
            else:
                row: int = key.row

            shift(row)

        else:
            logger.error(
                f"Добавляемый элемент {key} должен быть типа int или TableCell, "
                f"но получен {type(key)}")

    def switch(self, __from: int, __to: int):
        validate_under_limit_int(__from, len(self))
        validate_under_limit_int(__to, len(self))
        _cell_from: TableCell = self[__from]
        _cell_to: TableCell = self[__to]

        _cell_from.row, _cell_to.row = _cell_to.row, _cell_from.row

    def shift_hor(self, shift: int):
        for cell in iter(self):
            cell.shift_hor(shift)
        self._index += shift
