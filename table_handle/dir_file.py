# -*- coding: utf-8 -*-
from os.path import relpath
from pathlib import Path

from loguru import logger

from errors import LineInvalidTypeError
from table_handle.constants import FileExtension


class DirFile:
    """
    The file in the directory.

    Attributes
    ----------
    _root_dir : str or Path
        The path to the directory.
    _path: str or Path
        The path to the file relative to the directory.
    _content : list[str]
        The lines inside the file.
    _is_changed : bool
        The flag of changes in the file.
    """
    def __init__(self, root_dir: str | Path, path: str | Path):
        self._root_dir: Path = Path(root_dir).resolve()
        self._path: str = path
        self._content: list[str] = []
        self._is_changed: bool = False

    def __str__(self):
        return f"{self.full_path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.full_path})>"

    @property
    def full_path(self) -> Path:
        return self._root_dir.joinpath(self._path)

    @property
    def rel_path(self):
        return relpath(self.full_path, self._root_dir.parent.parent).replace("\\", "/")

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._content[item]
        else:
            raise LineInvalidTypeError(f"Тип должен быть int, но получен {type(item)}")

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise LineInvalidTypeError(
                f"Тип ключа должен быть int, а value - str, но получены {type(key)} и {type(value)}")

    def __iter__(self):
        return iter(self._content)

    def read(self) -> bool:
        """
        Reading the file content.

        The method tries to open the file and read each line.

        Handling UnicodeDecodeError, PermissionError, RuntimeError, FileNotFoundError, OSError.

        Returns
        -------
        bool
            The flag if the file contains text lines.
        """
        __flag: bool = False
        try:
            with open(self.full_path, "r+", encoding="utf-8") as f:
                self._content = f.readlines()
            __flag = True

        except UnicodeDecodeError as exc:
            logger.error(
                f"Ошибка декодирования символа, {exc.reason}\n"
                f"От {exc.start} до {exc.end}, в кодировке {exc.encoding}")

        except PermissionError:
            logger.error(f"Недостаточно прав для чтения файла {self.rel_path}")

        except RuntimeError:
            logger.error(f"Истекло время ожидания ответа во время чтения файла {self.rel_path}")

        except FileNotFoundError:
            logger.error(f"Файл {self.rel_path} был найден, но в течение работы скрипта был изменен")

        except OSError as exc:
            logger.error(
                f"Ошибка при работе с системой для файла {self.rel_path}, {exc.__class__.__name__}.\n"
                f"{exc.strerror}")

        finally:
            return __flag

    def write(self):
        """
        Writing the file content.

        The method tries to open the file and write all lines.

        Handling PermissionError, RuntimeError, FileNotFoundError, OSError.
        """
        _message: str = "В файл не внесены никакие изменения"

        if not self._is_changed:
            logger.info(f"В файле {self.rel_path} нет изменений")
            return

        try:
            with open(self.full_path, "w+", encoding="utf-8") as f:
                f.write("".join(self._content))
            _message = f"Файл {self.rel_path} записан\n\n"

        except PermissionError:
            logger.error(f"Недостаточно прав для записи в файл {self.rel_path}")

        except RuntimeError:
            logger.error(f"Истекло время ожидания ответа во время записи файла {self.rel_path}")

        except FileNotFoundError:
            logger.error(f"Файл {self.rel_path} был найден, но в течение работы скрипта был изменен")

        except OSError as exc:
            logger.error(
                f"Ошибка при работе с системой для файла {self.rel_path}, {exc.__class__.__name__}.\n"
                f"{exc.strerror}")

        finally:
            logger.info(_message, result=True)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    @property
    def extension(self) -> FileExtension:
        return FileExtension.dir_file(self.full_path.suffix)

    def get_file(self):
        if self.extension == FileExtension.ASCII_DOC:
            return AsciiDocFile(self._root_dir, self._path)
        elif self.extension == FileExtension.MARKDOWN:
            return MarkdownFile(self._root_dir, self._path)


class MarkdownFile(DirFile):
    pass


class AsciiDocFile(DirFile):
    pass
