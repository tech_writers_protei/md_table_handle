# -*- coding: utf-8 -*-
from __future__ import annotations
from typing import Iterable

from loguru import logger

from constants import TableCoordinate, validate_under_limit_int
from errors import InvalidHeadingError
from table_cell import TableCell
from table_column import TableColumn
from table_handle.dir_file import AsciiDocFile, DirFile, MarkdownFile
from table_row import HeaderRow, TableRow


class Table:
    def __init__(self, header: HeaderRow, name: str = None, cells: Iterable[TableCell] = None):
        if cells is None:
            cells: list[TableCell] = []

        if name is None:
            name: str = ""

        self._name: str = name
        self._header: HeaderRow = header
        self._cells: dict[TableCoordinate, TableCell] = {cell.coord: cell for cell in cells}
        self._rows: list[TableRow] = []
        self._columns: list[TableColumn] = []

    def __iter__(self):
        return iter(self._cells.values())

    def __len__(self):
        return len(self._header)

    @property
    def max_row(self):
        return max(cell.row for cell in iter(self))

    @property
    def max_column(self):
        return max(cell.column for cell in iter(self))

    def set_rows(self):
        _dict_row: dict[int, list[int]] = dict()

        for idx in range(self.max_row):
            _dict_row[idx] = []

        for cell in iter(self):
            _dict_row[cell.row].append(cell.index)

        for index, cell_id in _dict_row.items():
            cells: list[TableCell] = [TableCell.items.get(_id) for _id in cell_id]
            _row: TableRow = TableRow(index, cells)
            self._rows.append(_row)

    def set_columns(self):
        _dict_column: dict[int, list[int]] = dict()

        for idx in range(self.max_column):
            _dict_column[idx] = []

        for cell in iter(self):
            _dict_column[cell.column].append(cell.index)

        for index, cell_id in _dict_column.items():
            cells: list[TableCell] = [TableCell.items.get(_id) for _id in cell_id]
            _column: TableColumn = TableColumn(index, cells)
            self._columns.append(_column)

    def get_row(self, row: int) -> TableRow:
        validate_under_limit_int(row, self.max_row)
        return self._rows[row]

    def get_column(self, column: int | str) -> TableColumn:
        if isinstance(column, int):
            index: int = column
            validate_under_limit_int(index, self.max_column)

        elif isinstance(column, str):
            index: int = self.get_header_index(column)

        else:
            raise ValueError

        return self._columns[index]

    def get_cell(self, row: int, column: int) -> TableCell:
        table_coordinate: TableCoordinate = TableCoordinate(row, column)
        return self._cells.get(table_coordinate)

    @property
    def headings(self) -> list[str]:
        return [cell.text for cell in iter(self)]

    def get_header_index(self, heading: str) -> int:
        if heading not in self:
            logger.error(f"Столбец с названием {heading} не найден")
            raise InvalidHeadingError

        else:
            return self.headings.index(heading)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.headings

        elif isinstance(item, TableCell):
            return item in self._cells.values()

        elif isinstance(item, TableRow):
            return item in self._rows

        elif isinstance(item, TableColumn):
            return item in self._columns

        elif isinstance(item, TableCoordinate):
            return item in self._cells

        else:
            return False

    def __getitem__(self, item):
        if isinstance(item, int):
            validate_under_limit_int(item, len(self))
            return self.headings[item]

        elif isinstance(item, TableCoordinate):
            return self._cells.get(item)

        elif isinstance(item, tuple) and len(item) == 2:
            row, column = item[0], item[1]
            table_coord: TableCoordinate = TableCoordinate(row, column)
            return self._cells.get(table_coord)

        else:
            logger.error(
                f"Ключ {item} должен быть типа int, tuple[int, int] или TableCoordinate,"
                f" но получен {type(item)}")
            raise KeyError

    def set_row_cells(self):
        self._cells = dict()

        for row in self._rows:
            for cell in iter(row):
                self._cells[cell.coord] = cell

    def set_column_cells(self):
        self._cells = dict()

        for column in self._columns:
            for cell in iter(column):
                self._cells[cell.coord] = cell

    def get_columns_from(self, start: int) -> tuple[TableColumn, ...]:
        validate_under_limit_int(start, self.max_column)
        return *reversed([self.get_column(column) for column in range(start, self.max_column)]),

    def get_rows_from(self, start: int) -> tuple[TableRow, ...]:
        validate_under_limit_int(start, self.max_row)
        return *reversed([self.get_row(row) for row in range(start, self.max_row)]),

    def shift_hor(self, shift: int, start: int):
        for column in self.get_columns_from(start):
            column.shift_hor(shift)

    def shift_vert(self, shift: int, start: int):
        for row in self.get_rows_from(start):
            row.shift_vert(shift)

    def insert_row(self, index: int, row: TableRow):
        self.shift_vert(1, index)
        self._rows.insert(index, row)
        self.set_row_cells()

    def insert_column(self, index: int, column: TableColumn):
        self.shift_hor(1, index)
        self._columns.insert(index, column)
        self.set_column_cells()

    def join_rows(self, from_index: int, to_index: int):
        from_row: TableRow = self.get_row(from_index)
        to_row: TableRow = self.get_row(to_index)
        from_row + to_row
        self.set_row_cells()

    def join_columns(self, from_index: int, to_index: int):
        from_column: TableColumn = self.get_column(from_index)
        to_column: TableColumn = self.get_column(to_index)
        from_column + to_column
        self.set_column_cells()

    def __sub__(self, other):
        if isinstance(other, TableRow) and other in self._rows:
            self._rows.remove(other)
            self.shift_vert(-1, other.index)
            self.set_row_cells()

        elif isinstance(other, TableColumn) and other in self._columns:
            self._columns.remove(other)
            self.shift_hor(-1, other.index)
            self.set_column_cells()

        else:
            logger.error(f"Невозможно удалить элемент {other} типа {type(other)}")

    def split_row(self, index: int):
        table_row: TableRow = self.get_row(index)
        row: TableRow = table_row.duplicate()
        self.insert_row(index, row)

    def split_column(self, index: int):
        table_column: TableColumn = self.get_column(index)
        column: TableColumn = table_column.duplicate()
        self.insert_column(index, column)

    @property
    def name(self):
        raise NotImplementedError

    @name.setter
    def name(self, value):
        raise NotImplementedError

    def get_table(self, dir_file: DirFile):
        if isinstance(dir_file, MarkdownFile):
            return MdTable(self._header, self._cells.values())
        elif isinstance(dir_file, AsciiDocFile):
            return AsciiDocTable(self._header, None, self._cells.values())


class MdTable(Table):
    def __init__(self, header: HeaderRow, cells: Iterable[TableCell] = None):
        name: str | None = None
        super().__init__(header, name, cells)

    @property
    def name(self):
        return ""

    @name.setter
    def name(self, value):
        pass


class AsciiDocTable(Table):
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
