# -*- coding: utf-8 -*-
from pathlib import Path

from table_handle.dir_file import DirFile


def main(file_path: str | Path):
    file_path: Path = Path(file_path).resolve()
    root_dir: Path = file_path.parent
    path: str = file_path.name
    dir_file: DirFile = DirFile(root_dir, path)
    file = dir_file.get_file()
    file.read()
