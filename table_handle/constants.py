# -*- coding: utf-8 -*-
from enum import Enum, auto
from pathlib import Path
from typing import NamedTuple

from loguru import logger

from errors import ValueOutOfRangeError


def validate_over_one_int(value: int):
    if value < -1:
        logger.error(f"Значение {value} меньше -1")
        raise ValueOutOfRangeError


def validate_under_limit_int(value: int, limit: int):
    if value < 0 or value > limit:
        logger.error(f"Значение {value} отрицательное или превышает ограничение {limit}")
        raise ValueOutOfRangeError


class TableElement(Enum):
    ROW = "_column"
    COLUMN = "_row"

    def __str__(self):
        return f"{self._name_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"


class TableCoordinate(NamedTuple):
    row: int
    column: int

    def __str__(self):
        return f"({self.row}, {self.column})"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self.row}, {self.column})"


class FileExtension(Enum):
    MARKDOWN = auto
    ASCII_DOC = auto

    def __str__(self):
        return f"{self._name_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"

    @classmethod
    def dir_file(cls, path: str | Path):
        _dict_file_extension: dict[str, cls] = {
            ".md": cls.MARKDOWN,
            ".adoc": cls.ASCII_DOC
        }
        return _dict_file_extension.get(path.suffix)
